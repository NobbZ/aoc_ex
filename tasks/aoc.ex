defmodule Mix.Tasks.Aoc do
  use Mix.Task

  def run(args) do
    {args, [], []} = OptionParser.parse(args, strict: [spoil: :boolean, only: :string])

    solutions =
      case {args[:only], args[:except]} do
        {only, except} when is_binary(only) and is_binary(except) ->
          raise "'--only' and '--except' are mutually exclusive"

        {only, _} when is_binary(only) ->
          only |> parse |> filter(AocEx.solutions())

        {_, except} when is_binary(except) ->
          except |> parse |> reject(AocEx.solutions())

        {nil, nil} ->
          AocEx.solutions()
      end

    opts = %{spoil: args[:spoil] || false, solutions: solutions}

    AocEx.main(opts)
  end

  defp parse(str) do
    str
    |> String.trim()
    |> String.split(";")
    |> Enum.map(&String.split(&1, ":"))
    |> Enum.map(fn [y, days] ->
      y = String.to_integer(y)
      days = days |> String.split(",") |> Enum.map(&String.to_integer/1)
      {y, days}
    end)
  end

  defp filter(only, all) do
    all = Map.new(all)

    only
    |> Enum.reduce(%{}, fn {y, days}, acc ->
      kept_days =
        all[y]
        |> MapSet.new()
        |> MapSet.intersection(MapSet.new(days))
        |> MapSet.to_list()
        |> Enum.sort()

      Map.put(acc, y, kept_days)
    end)
    |> Enum.sort_by(&elem(&1, 0))
  end

  defp reject(_except, all) do
    all
  end
end
