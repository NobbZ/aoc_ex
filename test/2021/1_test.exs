defmodule AocEx.Y2021.D1Test do
  use AocEx.Case, part1: 72_766_415, part2: 15_132_814

  test "#{MUT}.part1(…) => 7" do
    assert 7 = D1.part1([199, 200, 208, 210, 200, 207, 240, 269, 260, 263])
  end

  test "#{MUT}.part2(…) => 5" do
    assert 5 = D1.part2([199, 200, 208, 210, 200, 207, 240, 269, 260, 263])
  end
end
