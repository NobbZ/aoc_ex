defmodule AocEx.Y2021.D2Test do
  use AocEx.Case, part1: 110_133_624, part2: 72_688_784

  test "#{MUT}.part1(…) => 150" do
    assert 150 = D2.part1(forward: 5, down: 5, forward: 8, up: 3, down: 8, forward: 2)
  end

  test "#{MUT}.part2(…) => 900" do
    assert 900 = D2.part2(forward: 5, down: 5, forward: 8, up: 3, down: 8, forward: 2)
  end
end
