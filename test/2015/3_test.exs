defmodule AocEx.Y2015.D3Test do
  use AocEx.Case, part1: 60_154_239, part2: 14_534_005

  example :part1, ">", 2
  example :part1, "^>v<", 4
  example :part1, "^v^v^v^v^v", 2

  example :part2, "^v", 3
  example :part2, "^>v<", 3
  example :part2, "^v^v^v^v^v", 11
end
