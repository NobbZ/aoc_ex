defmodule AocEx.Y2015.D2Test do
  use AocEx.Case, part1: 110_889_607, part2: 126_354_101

  example :part1, "2x3x4", 58
  example :part1, "1x1x10", 43
  example :part1, "2x3x4\n1x1x10", 101

  example :part2, "2x3x4", 34
  example :part2, "1x1x10", 14
  example :part2, "2x3x4\n1x1x10", 48
end
