defmodule AocEx.Y2015.D6Test do
  use AocEx.Case, part1: 36_012_651, part2: 87_280_618, tag: {:timeout, 5 * 60 * 1000}

  example :part1, "turn on 0,0 through 999,999", 1_000_000
  example :part1, "turn on 0,0 through 999,999\ntoggle 0,0 through 999,0", 999_000
  example :part1, "turn on 0,0 through 999,999\nturn off 499,499 through 500,500", 999_996

  example :part2, "turn on 0,0 through 0,0", 1
  example :part2, "toggle 0,0 through 999,999", 2_000_000
end
