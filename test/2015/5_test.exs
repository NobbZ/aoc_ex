defmodule AocEx.Y2015.D5Test do
  use AocEx.Case, part1: 44_734_653, part2: 118_107_429

  example :part1, "ugknbfddgicrmopn", 1
  example :part1, "aaa", 1
  example :part1, "jchzalrnumimnmhp", 0
  example :part1, "haegwjzuvuyypxyu", 0
  example :part1, "dvszwmarrgswjxmb", 0

  example :part2, "qjhvhtzxzqqjkmpb", 1
  example :part2, "xxyxx", 1
  example :part2, "uurcxstgmygtbstg", 0
  example :part2, "ieodomkazucvgmuy", 0
end
