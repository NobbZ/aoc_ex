defmodule AocEx.Y2015.D4Test do
  use AocEx.Case, part1: 73_723_580, part2: 91_803_504, tag: :md5
  @moduletag :md5

  example :part1, "abcdef", 609_043
  example :part1, "pqrstuv", 1_048_970

  example :part2, "abcdef", 6_742_839
  example :part2, "pqrstuv", 5_714_438
end
