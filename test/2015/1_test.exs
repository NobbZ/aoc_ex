defmodule AocEx.Y2015.D1Test do
  use AocEx.Case, part1: 94_007_609, part2: 64_690_429

  example :part1, "(())", 0
  example :part1, "()()", 0
  example :part1, "(((", 3
  example :part1, "(()(()(", 3
  example :part1, "))(((((", 3
  example :part1, "())", -1
  example :part1, "))(", -1
  example :part1, ")))", -3
  example :part1, ")())())", -3

  example :part2, ")", 1
  example :part2, "()())", 5
end
