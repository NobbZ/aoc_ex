defmodule AocEx.Y2018.D6Test do
  use AocEx.Case, part1: 56_110_540, part2: 63_330_809

  [{[{1, 1}, {1, 6}, {8, 3}, {3, 4}, {5, 5}, {8, 9}], 17}]
  |> Enum.each(fn {input, exp} ->
    test "#{MUT}.part1(#{inspect(input)}) => #{exp}",
      do: assert(unquote(exp) = D6.part1(unquote(input)))
  end)

  [{[{1, 1}, {1, 6}, {8, 3}, {3, 4}, {5, 5}, {8, 9}], 32, 16}]
  |> Enum.each(fn {input, threshold, exp} ->
    test "#{MUT}.part2(#{inspect(input)}) => #{exp}",
      do: assert(unquote(exp) = D6.part2(unquote(input), unquote(threshold)))
  end)
end
