defmodule AocEx.Y2018.D5Test do
  use AocEx.Case, part1: 33_192_714, part2: 109_624_982

  [{"dabAcCaCBAcCcaDA", 10}]
  |> Enum.each(fn {input, exp} ->
    test "#{MUT}.part1(example)) => #{exp}",
      do: assert(unquote(exp) = D5.part1(unquote(Macro.escape(input))))
  end)

  [{"dabAcCaCBAcCcaDA", 4}]
  |> Enum.each(fn {input, exp} ->
    test "#{MUT}.part2(example)) => #{exp}",
      do: assert(unquote(exp) = D5.part2(unquote(Macro.escape(input))))
  end)
end
