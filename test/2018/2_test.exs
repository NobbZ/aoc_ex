defmodule AocEx.Y2018.D2Test do
  use AocEx.Case, part1: 44_265_455, part2: 80_219_158

  [{["abcdef", "bababc", "abbcde", "abcccd", "aabcdd", "abcdee", "ababab"], 12}]
  |> Enum.each(fn {input, exp} ->
    test "#{D2}.part1(#{inspect(input)}) => #{exp}",
      do: assert(unquote(exp) = D2.part1(unquote(input)))
  end)

  [{["abcde", "fghij", "klmno", "pqrst", "fguij", "axcye", "wvxyz"], "fgij"}]
  |> Enum.each(fn {input, exp} ->
    test "#{D2}.part2(#{inspect(input)}) => #{exp}",
      do: assert(unquote(exp) = D2.part2(unquote(input)))
  end)
end
