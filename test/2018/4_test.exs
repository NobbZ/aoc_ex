defmodule AocEx.Y2018.D4Test do
  use AocEx.Case, part1: 85_030_819, part2: 129_728_166

  [
    {[
       {~N[1518-11-01 00:00:00], {:begin, 10}},
       {~N[1518-11-01 00:05:00], :start_sleep},
       {~N[1518-11-01 00:25:00], :stop_sleep},
       {~N[1518-11-01 00:30:00], :start_sleep},
       {~N[1518-11-01 00:55:00], :stop_sleep},
       {~N[1518-11-01 23:58:00], {:begin, 99}},
       {~N[1518-11-02 00:40:00], :start_sleep},
       {~N[1518-11-02 00:50:00], :stop_sleep},
       {~N[1518-11-03 00:05:00], {:begin, 10}},
       {~N[1518-11-03 00:24:00], :start_sleep},
       {~N[1518-11-03 00:29:00], :stop_sleep},
       {~N[1518-11-04 00:02:00], {:begin, 99}},
       {~N[1518-11-04 00:36:00], :start_sleep},
       {~N[1518-11-04 00:46:00], :stop_sleep},
       {~N[1518-11-05 00:03:00], {:begin, 99}},
       {~N[1518-11-05 00:45:00], :start_sleep},
       {~N[1518-11-05 00:55:00], :stop_sleep}
     ], 240}
  ]
  |> Enum.each(fn {input, exp} ->
    test "#{MUT}.part1(example)) => #{exp}",
      do: assert(unquote(exp) = D4.part1(unquote(Macro.escape(input))))
  end)

  [
    {[
       {~N[1518-11-01 00:00:00], {:begin, 10}},
       {~N[1518-11-01 00:05:00], :start_sleep},
       {~N[1518-11-01 00:25:00], :stop_sleep},
       {~N[1518-11-01 00:30:00], :start_sleep},
       {~N[1518-11-01 00:55:00], :stop_sleep},
       {~N[1518-11-01 23:58:00], {:begin, 99}},
       {~N[1518-11-02 00:40:00], :start_sleep},
       {~N[1518-11-02 00:50:00], :stop_sleep},
       {~N[1518-11-03 00:05:00], {:begin, 10}},
       {~N[1518-11-03 00:24:00], :start_sleep},
       {~N[1518-11-03 00:29:00], :stop_sleep},
       {~N[1518-11-04 00:02:00], {:begin, 99}},
       {~N[1518-11-04 00:36:00], :start_sleep},
       {~N[1518-11-04 00:46:00], :stop_sleep},
       {~N[1518-11-05 00:03:00], {:begin, 99}},
       {~N[1518-11-05 00:45:00], :start_sleep},
       {~N[1518-11-05 00:55:00], :stop_sleep}
     ], 4455}
  ]
  |> Enum.each(fn {input, exp} ->
    test "#{MUT}.part2(example)) => #{exp}",
      do: assert(unquote(exp) = D4.part2(unquote(Macro.escape(input))))
  end)
end
