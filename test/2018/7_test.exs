defmodule AocEx.Y2018.D7Test do
  use AocEx.Case, part1: 11_097_207

  [{%{?A => ~c[C], ?F => ~c[C], ?B => ~c[A], ?D => ~c[A], ?E => ~c[BDF]}, "CABDFE"}]
  |> Enum.each(fn {input, exp} ->
    test "#{MUT}.part1(#{inspect(input)}) => #{exp}",
      do: assert(unquote(exp) = D7.part1(unquote(Macro.escape(input))))
  end)
end
