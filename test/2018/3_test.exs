defmodule AocEx.Y2018.D3Test do
  use AocEx.Case, part1: 99_177_838, part2: 122_879_742

  [
    {[
       %{id: 1, coord: {1, 3}, size: {4, 4}},
       %{id: 2, coord: {3, 1}, size: {4, 4}},
       %{id: 3, coord: {5, 5}, size: {2, 2}}
     ], 4}
  ]
  |> Enum.each(fn {input, exp} ->
    test "#{MUT}.part1(#{inspect(input)}) => #{exp}",
      do: assert(unquote(exp) = D3.part1(unquote(Macro.escape(input))))
  end)

  [
    {[
       %{id: 1, coord: {1, 3}, size: {4, 4}},
       %{id: 2, coord: {3, 1}, size: {4, 4}},
       %{id: 3, coord: {5, 5}, size: {2, 2}}
     ], 3}
  ]
  |> Enum.each(fn {input, exp} ->
    test "#{MUT}.part2(#{inspect(input)}) => #{exp}",
      do: assert(unquote(exp) = D3.part2(unquote(Macro.escape(input))))
  end)
end
