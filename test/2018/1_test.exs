defmodule AocEx.Y2018.D1Test do
  use AocEx.Case, part1: 98_587_416, part2: 49_258_388

  [{[+1, -2, +3, +1], 3}, {[+1, +1, +1], 3}, {[+1, +1, -2], 0}, {[-1, -2, -3], -6}]
  |> Enum.each(fn {input, exp} ->
    test "#{D1}.part1(#{inspect(input)}) => #{exp}",
      do: assert(unquote(exp) = D1.part1(unquote(input)))
  end)

  [
    {[+1, -2, +3, +1], 2},
    {[+1, -1], 0},
    {[+3, +3, +4, -2, -4], 10},
    {[-6, +3, +8, +5, -6], 5},
    {[+7, +7, -2, -7, -4], 14}
  ]
  |> Enum.each(fn {input, exp} ->
    test "#{D1}.part2(#{inspect(input)}) => #{exp}",
      do: assert(unquote(exp) = D1.part2(unquote(input)))
  end)
end
