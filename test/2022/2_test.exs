defmodule AocEx.Y2022.D2Test do
  use AocEx.Case, part1: 30_072_800, part2: 93_913_713

  @input [rock: :paper, paper: :rock, scissors: :scissors]

  test "#{MUT}.part1(…) => 7" do
    assert 15 = MUT.part1(@input)
  end

  test "#{MUT}.part2(…) => 5" do
    assert 12 = MUT.part2(@input)
  end
end
