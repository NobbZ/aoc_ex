defmodule AocEx.Y2022.D4Test do
  use AocEx.Case, part1: 63_847_902, part2: 133_552_775

  @input """
         2-4,6-8
         2-3,4-5
         5-7,7-9
         2-8,3-7
         6-6,4-6
         2-6,4-8
         """
         |> String.trim()
         |> MUT.transform()

  test "#{MUT}.part1(…) => 2" do
    assert 2 = MUT.part1(@input)
  end

  test "#{MUT}.part2(…) => 4" do
    assert 4 = MUT.part2(@input)
  end
end
