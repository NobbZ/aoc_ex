defmodule AocEx.Y2022.D1Test do
  use AocEx.Case, part1: 31_719_468, part2: 131_478_440

  test "#{MUT}.part1(…) => 24000" do
    assert 24_000 =
             MUT.part1([[1000, 2000, 3000], [4000], [5000, 6000], [7000, 8000, 9000], [10_000]])
  end

  test "#{MUT}.part1(…) => 45000" do
    assert 45_000 =
             MUT.part2([[1000, 2000, 3000], [4000], [5000, 6000], [7000, 8000, 9000], [10_000]])
  end
end
