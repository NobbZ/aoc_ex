defmodule AocEx.Y2022.D3Test do
  use AocEx.Case, part1: 116_801_154, part2: 89_814_115

  @input """
         vJrwpWtwJgWrhcsFMMfFFhFp
         jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
         PmmdzqPrVvPwwTWBwg
         wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
         ttgJtRGJQctTZtZT
         CrZsJsPPZsGzwwsLwLmpwMDw
         """
         |> String.trim()
         |> MUT.transform()

  test "#{MUT}.part1(…) => 7" do
    assert 157 = MUT.part1(@input)
  end

  test "#{MUT}.part2(…) => 5" do
    assert 70 = MUT.part2(@input)
  end
end
