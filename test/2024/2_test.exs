defmodule AocEx.Y2024.D2Test do
  use AocEx.Case, part1: 80_418_153, part2: 85_925_560

  @input """
         7 6 4 2 1
         1 2 7 8 9
         9 7 6 2 1
         1 3 2 4 5
         8 6 4 4 1
         1 3 6 7 9
         """
         |> MUT.transform()

  test "#{MUT}.part1(…) => 2" do
    assert 2 = MUT.part1(@input)
  end

  test "#{MUT}.part2(…) => 4" do
    assert 4 = MUT.part2(@input)
  end
end
