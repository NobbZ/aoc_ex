defmodule AocEx.Y2024.D1Test do
  use AocEx.Case, part1: 96_716_409, part2: 47150299

  @input """
         3   4
         4   3
         2   5
         1   3
         3   9
         3   3
         """
         |> MUT.transform()

  test "#{MUT}.part1(…) => 11" do
    assert 11 = MUT.part1(@input)
  end

  test "#{MUT}.part2(…) => 31" do
    assert 31 = MUT.part2(@input)
  end
end
