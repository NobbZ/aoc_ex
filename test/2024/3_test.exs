defmodule AocEx.Y2024.D3Test do
  use AocEx.Case, part1: 103_762_827, part2: 26_948_320

  @input1 "xmul(2,4)%&mul[3,7]!@^do_not_mul(5,5)+mul(32,64]then(mul(11,8)mul(8,5))"
          |> MUT.transform()

  @input2 "xmul(2,4)&mul[3,7]!^don't()_mul(5,5)+mul(32,64](mul(11,8)undo()?mul(8,5))"
          |> MUT.transform()

  test "#{MUT}.part1(…) => 161" do
    assert 161 = MUT.part1(@input1)
  end

  test "#{MUT}.part2(…) => 48" do
    assert 48 = MUT.part2(@input2)
  end
end
