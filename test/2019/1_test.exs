defmodule AocEx.Y2019.D1Test do
  use AocEx.Case, part1: 108_297_674, part2: 111_840_843

  [
    {[12], 2},
    {[14], 2},
    {[1969], 654},
    {[100_756], 33_583}
  ]
  |> Enum.each(fn {input, exp} ->
    test "#{MUT}.part1(#{inspect(input)}) => #{exp}",
      do: assert(unquote(exp) = D1.part1(unquote(input)))
  end)

  [{[14], 2}, {[1969], 966}, {[100_756], 50_346}]
  |> Enum.each(fn {input, exp} ->
    test "#{MUT}.part2(#{inspect(input)}) => #{exp}",
      do: assert(unquote(exp) = D1.part2(unquote(input)))
  end)
end
