defmodule AocEx.Y2019.D2Test do
  use AocEx.Case, part1: 72_478_024, part2: 17_244_970, tag: :intcode

  alias AocEx.Y2019.IntcodeVM

  [
    {[1, 0, 0, 0, 99], [2, 0, 0, 0, 99]},
    {[2, 3, 0, 3, 99], [2, 3, 0, 6, 99]},
    {[2, 4, 4, 5, 99, 0], [2, 4, 4, 5, 99, 9801]},
    {[1, 1, 1, 4, 99, 5, 6, 0, 99], [30, 1, 1, 4, 2, 5, 6, 0, 99]}
  ]
  |> Enum.each(fn {input, exp} ->
    @tag :intcode
    test "#{MUT}.part1(#{inspect(input)}) => #{inspect(exp)}" do
      exp = IntcodeVM.new(unquote(exp))
      res = IntcodeVM.exec(unquote(input |> IntcodeVM.new() |> Macro.escape()))

      assert exp.mem == res.mem
    end
  end)
end
