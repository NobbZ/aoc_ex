defmodule AocEx.Y2019.D9Test do
  use AocEx.Case, part1: 20_258_451, part2: 113_502_936, tag: :intcode

  describe "additional tests for the VM" do
    @tag :intcode
    test "program creates itself as output" do
      output =
        "109,1,204,-1,1001,100,1,100,1008,100,16,101,1006,101,0,99"
        |> String.split(",")
        |> Enum.map(&String.to_integer/1)
        |> AocEx.Y2019.IntcodeVM.new()
        |> AocEx.Y2019.IntcodeVM.exec()
        |> AocEx.Y2019.IntcodeVM.output_to_list()
        |> elem(1)

      exp = [109, 1, 204, -1, 1001, 100, 1, 100, 1008, 100, 16, 101, 1006, 101, 0, 99]

      assert exp == output
    end
  end
end
