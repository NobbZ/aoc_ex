defmodule AocEx.Y2019.D3Test do
  use AocEx.Case, part1: 59_662_826, part2: 115_453_625

  [
    {[[right: 8, up: 5, left: 5, down: 3], [up: 7, right: 6, down: 4, left: 4]], 6},
    {[
       [right: 75, down: 30, right: 83, up: 83, left: 12, down: 49, right: 71, up: 7, left: 72],
       [up: 62, right: 66, up: 55, right: 34, down: 71, right: 55, down: 58, right: 83]
     ], 159},
    {[
       [
         right: 98,
         up: 47,
         right: 26,
         down: 63,
         right: 33,
         up: 87,
         left: 62,
         down: 20,
         right: 33,
         up: 53,
         right: 51
       ],
       [
         up: 98,
         right: 91,
         down: 20,
         right: 16,
         down: 67,
         right: 40,
         up: 7,
         right: 15,
         up: 6,
         right: 7
       ]
     ], 135}
  ]
  |> Enum.each(fn {input, exp} ->
    test "#{MUT}.part1(#{inspect(input)}) => #{exp}",
      do: assert(unquote(exp) = D3.part1(unquote(input)))
  end)

  [
    {[[right: 8, up: 5, left: 5, down: 3], [up: 7, right: 6, down: 4, left: 4]], 30},
    {[
       [right: 75, down: 30, right: 83, up: 83, left: 12, down: 49, right: 71, up: 7, left: 72],
       [up: 62, right: 66, up: 55, right: 34, down: 71, right: 55, down: 58, right: 83]
     ], 610},
    {[
       [
         right: 98,
         up: 47,
         right: 26,
         down: 63,
         right: 33,
         up: 87,
         left: 62,
         down: 20,
         right: 33,
         up: 53,
         right: 51
       ],
       [
         up: 98,
         right: 91,
         down: 20,
         right: 16,
         down: 67,
         right: 40,
         up: 7,
         right: 15,
         up: 6,
         right: 7
       ]
     ], 410}
  ]
  |> Enum.each(fn {input, exp} ->
    test "#{MUT}.part2(#{inspect(input)}) => #{exp}",
      do: assert(unquote(exp) = D3.part2(unquote(input)))
  end)
end
