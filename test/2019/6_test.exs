defmodule AocEx.Y2019.D6Test do
  use AocEx.Case, part1: 27_478_789, part2: 121_246_014

  [{"COM)B\nB)C\nC)D\nD)E\nE)F\nB)G\nG)H\nD)I\nE)J\nJ)K\nK)L", 42}]
  |> Enum.each(fn {input, exp} ->
    test "#{MUT}.part1(#{inspect(input)}) => #{exp}",
      do: assert(unquote(exp) = D6.part1(D6.transform(unquote(input))))
  end)

  [{"COM)B\nB)C\nC)D\nD)E\nE)F\nB)G\nG)H\nD)I\nE)J\nJ)K\nK)L\nK)YOU\nI)SAN", 4}]
  |> Enum.each(fn {input, exp} ->
    test "#{MUT}.part2(#{inspect(input)}) => #{exp}",
      do: assert(unquote(exp) = D6.part2(D6.transform(unquote(input))))
  end)
end
