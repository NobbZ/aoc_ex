defmodule AocEx.Y2019.D5Test do
  use AocEx.Case, part1: 35_411_089, part2: 123_139_258, tag: :intcode

  # [{1002, 2, [:position, :immediate]}, {102, 2, [:immediate, :position]}]
  # |> Enum.each(fn {val, arity, exp} ->
  #   test "#{val} of arity #{arity} produces correct access_modes of #{inspect(exp)}" do
  #     alias AocEx.Y2019.IntcodeVM, as: VM
  #     assert unquote(exp) == unquote(val) |> VM.parse_intcode() |> VM.for_arity(unquote(arity))
  #   end
  # end)
end
