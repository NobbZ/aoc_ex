defmodule AocEx.Case do
  defmacro __using__(opts) do
    {mut, day} = parse_test_module(__CALLER__.module)

    part1_exp = opts[:part1]
    part2_exp = opts[:part2]

    tag =
      case opts[:tag] do
        nil ->
          quote do
            nil
          end

        {key, value} ->
          quote do
            @tag [{unquote(key), unquote(value)}]
          end

        key ->
          quote do
            @tag [{unquote(key), true}]
          end
      end

    quote do
      use ExUnit.Case

      alias unquote(mut)
      alias unquote(mut), as: MUT

      import unquote(__MODULE__)

      @day unquote(day)
      @module_under_test unquote(mut)

      describe "#{unquote(mut)} solutions" do
        if unquote(part1_exp) do
          unquote(tag)

          test "part1 => #{inspect(unquote(part1_exp))}" do
            assert unquote(part1_exp) == :erlang.phash2(MUT.part1())
          end
        end

        if unquote(part2_exp) do
          unquote(tag)

          test "part2 => #{inspect(unquote(part2_exp))}" do
            assert unquote(part2_exp) == :erlang.phash2(MUT.part2())
          end
        end
      end
    end
  end

  defmacro example(part, input, expect) do
    {mut, _} = parse_test_module(__CALLER__.module)

    call = "#{mut}.#{part}(#{inspect(input)})"

    quote do
      test "#{unquote(call)} => #{inspect(unquote(expect))}" do
        input = @module_under_test.transform(unquote(input))
        assert unquote(expect) == apply(@module_under_test, unquote(part), [input])
      end
    end
  end

  defp parse_test_module(test_module) do
    ["AocEx", year, day] = Module.split(test_module)

    day = String.replace_suffix(day, "Test", "")

    module_under_test = Module.concat(["AocEx", year, day])

    {module_under_test, day}
  end
end
