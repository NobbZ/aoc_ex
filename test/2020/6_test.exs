defmodule AocEx.Y2020.D6Test do
  use AocEx.Case, part1: 9_365_797, part2: 125_064_234

  @many_groups """
  abc

  a
  b
  c

  ab
  ac

  a
  a
  a
  a

  b
  """

  example :part1, "abcx\nabcy\nabcz", 6
  example :part1, @many_groups, 11

  example :part2, @many_groups, 6
end
