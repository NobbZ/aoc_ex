defmodule AocEx.Y2020.D1Test do
  use AocEx.Case, part1: 10_546_261, part2: 57_702_629

  [{[1721, 979, 366, 299, 675, 1456], 514_579}]
  |> Enum.each(fn {input, exp} ->
    test "#{MUT}.part1(#{inspect(input)}) => #{exp}",
      do: assert(unquote(exp) = D1.part1(unquote(input)))
  end)

  [{[1721, 979, 366, 299, 675, 1456], 241_861_950}]
  |> Enum.each(fn {input, exp} ->
    test "#{MUT}.part2(#{inspect(input)}) => #{exp}",
      do: assert(unquote(exp) = D1.part2(unquote(input)))
  end)
end
