defmodule AocEx.Y2020.D2Test do
  use AocEx.Case, part1: 14_924_812, part: 24_996_580

  example :part1,
          """
          1-3 a: abcde
          1-3 b: cdefg
          2-9 c: ccccccccc
          """,
          2

  example :part2,
          """
          1-3 a: abcde
          1-3 b: cdefg
          2-9 c: ccccccccc
          """,
          1
end
