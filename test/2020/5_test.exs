defmodule AocEx.Y2020.D5Test do
  use AocEx.Case, part1: 35_982_933, part2: 22_475_412

  example :part1, "FBFBBFFRLR", 357
  example :part1, "BFFFBBFRRR", 567
  example :part1, "FFFBBBFRRR", 119
  example :part1, "BBFFBBFRLL", 820
  example :part1, "FBFBBFFRLR\nBFFFBBFRRR\nFFFBBBFRRR\nBBFFBBFRLL\n", 820
end
