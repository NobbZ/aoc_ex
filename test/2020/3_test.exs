defmodule AocEx.Y2020.D3Test do
  use AocEx.Case, part1: 48_023_907, part2: 6_129_665

  @example """
  ..##.......
  #...#...#..
  .#....#..#.
  ..#.#...#.#
  .#...##..#.
  ..#.##.....
  .#.#.#....#
  .#........#
  #.##...#...
  #...##....#
  .#..#...#.#
  """

  example :part1, @example, 7

  example :part2, @example, 336
end
