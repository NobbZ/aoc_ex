{
  inputs.nixpkgs.url = "github:nixos/nixpkgs?ref=nixpkgs-unstable";
  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, flake-utils, ... }:
    flake-utils.lib.eachDefaultSystem (system:
      let pkgs = import nixpkgs { inherit system; };
          inherit (pkgs) glibcLocales;
          inherit (pkgs.stdenv) isLinux;
          inherit (pkgs.lib) optionalString;

          beamPkgs = with pkgs.beam_minimal; packagesWith interpreters.erlang_27;
          erlang = beamPkgs.erlang;
          elixir = beamPkgs.elixir_1_17;
      in {
        devShell = pkgs.mkShell {
          packages = [ elixir pkgs.nil pkgs.elixir-ls pkgs.inotify-tools ];

          LOCALE_ARCHIVE = optionalString isLinux "${glibcLocales}/lib/locale/locale-archive";
          LANG = "en_US.UTF-8";

          ERL_INCLUDE_PATH = "${erlang}/lib/erlang/usr/include";
        };
      }
    );
}
