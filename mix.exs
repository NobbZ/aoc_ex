defmodule AocEx.MixProject do
  use Mix.Project

  def project do
    [
      app: :aoc_ex,
      version: "0.1.0",
      elixir: "~> 1.7",
      start_permanent: Mix.env() == :prod,
      elixirc_paths: elixirc_paths(Mix.env()),
      preferred_cli_env: [muzak: :test],
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  defp elixirc_paths(:dev), do: ["tasks" | elixirc_paths(:default)]
  defp elixirc_paths(:test), do: ["test/helpers" | elixirc_paths(:default)]
  defp elixirc_paths(:default), do: ["lib"]
  defp elixirc_paths(_), do: elixirc_paths(:default)

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:credo, "~> 1.6", only: :dev},
      {:dialyxir, "~> 1.2", only: :dev, runtime: false},
      {:muzak, "~> 1.1"},
      {:timex, "~> 3.6"},
      {:mix_test_watch, "~> 1.0", only: [:dev, :test], runtime: false}
    ]
  end
end
