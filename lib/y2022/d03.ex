defmodule AocEx.Y2022.D3 do
  use AocEx.Boilerplate,
    file: "priv/y2022/d03.txt",
    transform: fn raw ->
      raw
      |> String.split("\n")
      |> Enum.map(&String.to_charlist/1)
      |> Enum.map(&Enum.split(&1, div(length(&1), 2)))
    end

  def part1(input \\ processed()) do
    input
    |> Enum.map(fn {left, right} -> {MapSet.new(left), MapSet.new(right)} end)
    |> Enum.map(fn {left, right} -> MapSet.intersection(left, right) end)
    |> Enum.map(fn set -> set |> Enum.to_list() |> hd() end)
    |> Enum.map(&score/1)
    |> Enum.sum()
  end

  def part2(input \\ processed()) do
    input
    |> Enum.chunk_every(3)
    |> Enum.flat_map(fn group ->
      [a, b, c] =
        group
        |> Enum.map(&Tuple.to_list/1)
        |> Enum.map(&Enum.concat/1)
        |> Enum.map(&MapSet.new/1)

      a
      |> MapSet.intersection(b)
      |> MapSet.intersection(c)
      |> Enum.to_list()
    end)
    |> Enum.map(&score/1)
    |> Enum.sum()
  end

  defp score(c) when c in ?a..?z, do: c - ?a + 1
  defp score(c) when c in ?A..?Z, do: c - ?A + 27
end
