defmodule AocEx.Y2022.D4 do
  use AocEx.Boilerplate,
    file: "priv/y2022/d04.txt",
    transform: fn raw ->
      raw
      |> String.split("\n")
      |> Enum.map(fn line ->
        line
        |> String.split(",")
        |> Enum.map(fn assignments ->
          assignments
          |> String.split("-")
          |> Enum.map(&String.to_integer/1)
          |> then(fn [first, last] -> Range.new(first, last) end)
          |> MapSet.new()
        end)
      end)
    end

  def part1(input \\ processed()) do
    input
    |> Enum.map(fn [left, right] -> {left, right, MapSet.intersection(left, right)} end)
    |> Enum.filter(fn {left, right, intersection} -> intersection in [left, right] end)
    |> Enum.count()
  end

  def part2(input \\ processed()) do
    input
    |> Enum.map(fn [left, right] -> MapSet.intersection(left, right) end)
    |> Enum.reject(&Enum.empty?/1)
    |> Enum.count()
  end
end
