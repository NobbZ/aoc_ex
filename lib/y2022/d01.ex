defmodule AocEx.Y2022.D1 do
  use AocEx.Boilerplate,
    file: "priv/y2022/d01.txt",
    transform: fn raw ->
      raw
      |> String.split("\n\n")
      |> Enum.map(fn elf -> elf |> String.split("\n") |> Enum.map(&String.to_integer/1) end)
    end

  def part1(input \\ processed()) do
    input
    |> Enum.map(&Enum.sum/1)
    |> Enum.max()
  end

  def part2(input \\ processed()) do
    input
    |> Enum.map(&Enum.sum/1)
    |> Enum.sort(:desc)
    |> Enum.take(3)
    |> Enum.sum()
  end
end
