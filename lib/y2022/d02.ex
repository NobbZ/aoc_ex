defmodule AocEx.Y2022.D2 do
  use AocEx.Boilerplate,
    file: "priv/y2022/d02.txt",
    transform: fn raw ->
      raw
      |> String.split("\n")
      |> Enum.map(&String.split(&1, " "))
      |> Enum.map(fn
        ["A", x] -> {:rock, x}
        ["B", x] -> {:paper, x}
        ["C", x] -> {:scissors, x}
      end)
      |> Enum.map(fn
        {x, "X"} -> {x, :rock}
        {x, "Y"} -> {x, :paper}
        {x, "Z"} -> {x, :scissors}
      end)
    end

  @win 6
  @draw 3
  @lose 0

  @shapes %{rock: 1, paper: 2, scissors: 3}

  @stronger_than %{rock: :paper, paper: :scissors, scissors: :rock}
  @weaker_than Map.new(@stronger_than, fn {x, y} -> {y, x} end)

  def part1(input \\ processed()) do
    input
    |> Enum.map(fn
      {:rock, :scissors} -> @lose + @shapes.scissors
      {:scissors, :paper} -> @lose + @shapes.paper
      {:paper, :rock} -> @lose + @shapes.rock
      {shape, shape} -> @draw + @shapes[shape]
      {_, shape} -> @win + @shapes[shape]
    end)
    |> Enum.sum()
  end

  def part2(input \\ processed()) do
    input
    |> Enum.map(fn
      {shape, :rock} -> {shape, :lose}
      {shape, :paper} -> {shape, :draw}
      {shape, :scissors} -> {shape, :win}
    end)
    |> Enum.map(fn
      {opponent, :lose} -> @lose + @shapes[@weaker_than[opponent]]
      {opponent, :draw} -> @draw + @shapes[opponent]
      {opponent, :win} -> @win + @shapes[@stronger_than[opponent]]
    end)
    |> Enum.sum()
  end
end
