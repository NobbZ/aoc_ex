defmodule AocEx do
  @solutions [{2015, 1..6}, {2018, 1..7}, {2019, 1..9}, {2020, 1..6}, {2021, 1..2}, {2022, 1..4}, {2024, 1..3}]

  @solutions
  |> Enum.each(fn {year, _} ->
    m = Module.concat(__MODULE__, "Y#{year}")

    defmodule m do
      def main(opts, days) do
        days
        |> Enum.each(fn day ->
          m = Module.concat(__MODULE__, "D#{day}")
          m.main(opts)
        end)
      end
    end
  end)

  def main(opts) do
    opts.solutions
    |> Enum.each(fn {year, days} ->
      m = Module.concat(__MODULE__, "Y#{year}")
      m.main(opts, days)
    end)
  end

  def solutions(), do: @solutions
end
