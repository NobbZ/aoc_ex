defmodule AocEx.Y2018.D4 do
  use AocEx.Boilerplate,
    file: "priv/y2018/d4.txt",
    transform: fn raw ->
      raw
      |> String.split("\n")
      |> Enum.sort()
      |> Enum.map(fn
        <<"[", date::binary-size(10), " ", time::binary-size(5), "] ", comment::binary>> ->
          {date, time, comment}
      end)
      |> Enum.map(fn {date, time, comment} ->
        {:ok, date} =
          date
          |> String.split("-")
          |> Enum.map(&String.to_integer/1)
          |> (&apply(Date, :new, &1)).()

        {:ok, time} =
          time
          |> String.split(":")
          |> Enum.map(&String.to_integer/1)
          |> (fn [h, m] -> Time.new(h, m, 0) end).()

        comment =
          case comment do
            <<"Guard #", id::binary-size(1), " begins shift">> -> {:begin, String.to_integer(id)}
            <<"Guard #", id::binary-size(2), " begins shift">> -> {:begin, String.to_integer(id)}
            <<"Guard #", id::binary-size(3), " begins shift">> -> {:begin, String.to_integer(id)}
            <<"Guard #", id::binary-size(4), " begins shift">> -> {:begin, String.to_integer(id)}
            "falls asleep" -> :start_sleep
            "wakes up" -> :stop_sleep
          end

        {:ok, date_time} = NaiveDateTime.new(date, time)

        {date_time, comment}
      end)
    end

  def part1(input \\ processed()) do
    {sums, minutes} =
      input
      |> consolidate_times()
      |> Enum.reduce({%{}, %{}}, fn
        {id, start..stop = asleep}, {sum_map, range_map} ->
          time_asleep = stop - start
          sum_map = Map.update(sum_map, id, time_asleep, &(&1 + time_asleep))
          range_map = Map.update(range_map, id, [asleep], &[asleep | &1])

          {sum_map, range_map}
      end)

    {guard, _} = Enum.max_by(sums, fn {_, mins} -> mins end)

    {minute, _} =
      minutes[guard]
      |> Enum.reduce(%{}, fn range, acc ->
        Enum.reduce(range, acc, fn m, acc -> Map.update(acc, m, 1, &(&1 + 1)) end)
      end)
      |> Enum.max_by(fn {_, count} -> count end)

    guard * minute
  end

  def part2(input \\ processed()) do
    {minute, {id, _}} =
      input
      |> consolidate_times()
      |> aggregate_asleeps()
      |> Enum.map(fn {minute, asleep_counts} ->
        {minute, Enum.max_by(asleep_counts, fn {_, times} -> times end)}
      end)
      |> Enum.max_by(fn {_, {_, times}} -> times end)

    minute * id
  end

  defp consolidate_times(list) do
    list
    |> Enum.flat_map_reduce(nil, fn
      {_, {:begin, id}}, old_id when is_integer(old_id) or old_id == nil ->
        {[], id}

      {start, :start_sleep}, id when is_integer(id) ->
        {[], {id, start}}

      {stop, :stop_sleep}, {id, start} when is_integer(id) ->
        {[{id, start.minute..(stop.minute - 1)}], id}
    end)
    |> elem(0)
  end

  defp aggregate_asleeps(list) do
    Enum.reduce(list, %{}, &aggregator/2)
  end

  defp aggregator({id, start..stop}, map) do
    Enum.reduce(start..stop, map, fn minute, map ->
      Map.update(map, minute, %{id => 1}, fn counts ->
        Map.update(counts, id, 1, &(&1 + 1))
      end)
    end)
  end
end
