defmodule AocEx.Y2018.D7 do
  use AocEx.Boilerplate,
    file: "priv/y2018/d7.txt",
    transform: fn raw ->
      raw
      |> String.split("\n")
      |> Enum.map(fn
        "Step " <> <<pre::utf8>> <> " must be finished before step " <> <<step::utf8>> <> _ ->
          {pre, step}
      end)
      |> Enum.group_by(&elem(&1, 1), &elem(&1, 0))
    end

  def part1(input \\ processed()) do
    input
    |> Stream.unfold(fn acc ->
      case get_next_workitem(acc) do
        nil ->
          nil

        work_item ->
          {work_item, remove_work_item(acc, work_item)}
      end
    end)
    |> Enum.into([])
    |> to_string()
  end

  def part2(_input \\ processed()) do
  end

  defp get_next_workitem(map) do
    case map_size(map) do
      0 ->
        nil

      1 ->
        map |> Map.keys() |> hd()

      _ ->
        map
        |> Map.values()
        |> Enum.concat()
        |> Enum.uniq()
        |> Enum.filter(&(map[&1] in [nil, []]))
        |> Enum.sort()
        |> hd
    end
  end

  defp remove_work_item(map, work_item) do
    map
    |> Enum.map(fn {k, l} -> {k, Enum.reject(l, &(&1 == work_item))} end)
    |> Map.new()
    |> Map.pop(work_item)
    |> elem(1)
  end
end
