defmodule AocEx.Y2018.D5 do
  use AocEx.Boilerplate, file: "priv/y2018/d5.txt"

  @delta ?a - ?A

  def part1(input \\ processed()) do
    input
    |> implode()
    |> byte_size()
  end

  def part2(input \\ processed()) do
    input
    |> String.downcase()
    |> to_charlist()
    |> Stream.uniq()
    |> Stream.map(&remove(input, &1))
    |> Stream.map(&implode/1)
    |> Enum.min_by(&byte_size/1)
    |> byte_size()
  end

  defp implode(str, acc \\ [])

  defp implode("", acc), do: acc |> Enum.reverse() |> to_string()

  defp implode(<<c1::utf8, c2::utf8, t::binary>>, acc) when @delta in [c1 - c2, c2 - c1],
    do: implode(t, acc)

  defp implode(<<c1::utf8, t::binary>>, [c2 | acc]) when @delta in [c1 - c2, c2 - c1],
    do: implode(t, acc)

  defp implode(<<c::utf8, t::binary>>, acc), do: implode(t, [c | acc])

  defp remove(str, char, acc \\ "")
  defp remove("", _, acc), do: acc
  defp remove(<<c::utf8, t::binary>>, c, acc), do: remove(t, c, acc)
  defp remove(<<uc::utf8, t::binary>>, lc, acc) when lc - uc == @delta, do: remove(t, lc, acc)
  defp remove(<<c::utf8, t::binary>>, char, acc), do: remove(t, char, acc <> <<c::utf8>>)
end
