defmodule AocEx.Y2018.D2 do
  use AocEx.Boilerplate,
    file: "priv/y2018/d2.txt",
    transform: fn raw -> raw |> String.split("\n") end

  def part1(input \\ processed()) do
    {two, three} =
      Enum.reduce(input, {0, 0}, fn id, {two, three} ->
        {c2, c3} =
          id
          |> String.to_charlist()
          |> Enum.group_by(& &1)
          |> Enum.reduce({false, false}, fn
            {_, [_, _]}, {_, three} -> {true, three}
            {_, [_, _, _]}, {two, _} -> {two, true}
            _, acc -> acc
          end)

        {if(c2, do: two + 1, else: two), if(c3, do: three + 1, else: three)}
      end)

    two * three
  end

  def part2(input \\ processed()) do
    input
    |> Enum.map(&String.to_charlist/1)
    |> Enum.reduce_while([], fn word, words ->
      words
      |> Enum.find(fn w -> hamming(word, w) == 1 end)
      |> case do
        nil -> {:cont, [word | words]}
        other -> {:halt, {word, other}}
      end
    end)
    |> Tuple.to_list()
    |> Enum.zip()
    |> Enum.filter(fn {a, b} -> a == b end)
    |> Enum.map(fn {c, c} -> c end)
    |> List.to_string()
  end

  defp hamming(a, b) do
    a |> Enum.zip(b) |> Enum.count(fn {a, b} -> a != b end)
  end
end
