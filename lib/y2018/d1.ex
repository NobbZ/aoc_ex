defmodule AocEx.Y2018.D1 do
  use AocEx.Boilerplate,
    file: "priv/y2018/d1.txt",
    transform: fn raw -> raw |> String.split("\n") |> Enum.map(&String.to_integer(&1)) end

  def part1(input \\ processed()) do
    input |> Enum.sum()
  end

  def part2(input \\ processed()) do
    input
    |> Stream.cycle()
    |> Enum.reduce_while({0, MapSet.new([0])}, fn n, {f, set} ->
      f = f + n

      if MapSet.member?(set, f) do
        {:halt, f}
      else
        {:cont, {f, MapSet.put(set, f)}}
      end
    end)
  end
end
