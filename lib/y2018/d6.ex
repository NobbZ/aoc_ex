defmodule AocEx.Y2018.D6 do
  use AocEx.Boilerplate,
    file: "priv/y2018/d6.txt",
    transform: fn raw ->
      raw
      |> String.split("\n")
      |> Enum.map(fn line ->
        line |> String.split(", ") |> Enum.map(&String.to_integer/1) |> List.to_tuple()
      end)
    end

  def part1(input \\ processed()) do
    {x1, y1, x2, y2} = bounding = find_bounding_box(input)

    bounding
    |> inner_coordinates()
    |> Enum.flat_map(fn pos ->
      input
      |> Enum.reduce(nil, fn
        coord, nil ->
          {manhattan_distance(pos, coord), [coord]}

        coord, old ->
          new_dist = manhattan_distance(pos, coord)

          update_entry(new_dist, coord, old)
      end)
      |> case do
        {_dist, [center]} -> [{center, pos}]
        {_dist, _} -> []
      end
    end)
    |> Enum.group_by(&elem(&1, 0), &elem(&1, 1))
    |> Enum.reject(fn {{x, y}, fields} ->
      Enum.any?([{x1 - 1, y}, {x2 + 1, y}, {x, y1 - 1}, {x, y2 + 1}], &(&1 in fields))
    end)
    |> Enum.map(fn {_, fields} -> length(fields) end)
    |> Enum.max()
  end

  def part2(input \\ processed(), threshold \\ 10_000) do
    input
    |> find_bounding_box
    |> inner_coordinates()
    |> Enum.count(fn pos ->
      dist_sum = input |> Enum.map(&manhattan_distance(pos, &1)) |> Enum.sum()

      dist_sum < threshold
    end)
  end

  defp inner_coordinates({x1, y1, x2, y2}) do
    for x <- (x1 - 1)..(x2 + 1), y <- (y1 - 1)..(y2 + 1), do: {x, y}
  end

  defp manhattan_distance({x1, y1}, {x2, y2}), do: abs(x1 - x2) + abs(y1 - y2)

  defp update_entry(new_dist, coord, {dist, coords} = old) do
    cond do
      dist < new_dist -> old
      dist == new_dist -> {dist, [coord | coords]}
      dist > new_dist -> {new_dist, [coord]}
    end
  end

  defp find_bounding_box([{x, y} | list]) do
    Enum.reduce(list, {x, y, x, y}, fn {x, y}, {x1, y1, x2, y2} ->
      {min(x, x1), min(y, y1), max(x, x2), max(y, y2)}
    end)
  end
end
