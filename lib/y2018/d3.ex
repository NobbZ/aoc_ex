defmodule AocEx.Y2018.D3 do
  use AocEx.Boilerplate,
    file: "priv/y2018/d3.txt",
    transform: fn raw ->
      raw
      |> String.split("\n")
      |> Enum.map(&String.split/1)
      |> Enum.map(fn ["#" <> id, "@", coord, size] ->
        id = String.to_integer(id)

        coord =
          coord
          |> String.trim(":")
          |> String.split(",")
          |> Enum.map(&String.to_integer/1)
          |> List.to_tuple()

        size =
          size
          |> String.split("x")
          |> Enum.map(&String.to_integer/1)
          |> List.to_tuple()

        %{id: id, coord: coord, size: size}
      end)
    end

  def part1(input \\ processed()) do
    input
    |> claims_per_square_inch()
    |> Enum.count(fn {_, n} -> n >= 2 end)
  end

  def part2(input \\ processed()) do
    claim_map = claims_per_square_inch(input)

    Enum.find(input, fn %{coord: coord, size: size} ->
      claims(coord, size)
      |> Enum.all?(fn pos -> claim_map[pos] == 1 end)
    end)
    |> (& &1[:id]).()
  end

  defp claims_per_square_inch(input) do
    Enum.reduce(input, %{}, fn %{coord: coord, size: size}, acc ->
      claims(coord, size)
      |> Enum.reduce(acc, fn pos, acc -> Map.update(acc, pos, 1, &(&1 + 1)) end)
    end)
  end

  defp claims({x, y}, {width, height}) do
    for x <- x..(x + width - 1), y <- y..(y + height - 1), do: {x, y}
  end
end
