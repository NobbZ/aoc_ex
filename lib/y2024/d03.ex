defmodule AocEx.Y2024.D3.Parser do
  def parse(code) do
    case code do
      "mul(" <> rest -> parse_mul(rest)
      "don't()" <> rest -> [:dont | parse(rest)]
      "do()" <> rest -> [:do | parse(rest)]
      <<_::utf8, rest::binary>> -> parse(rest)
      "" -> []
    end
  end

  defp parse_mul(rest) do
    with {lhs, rest} <- Integer.parse(rest),
         <<?,, rest::binary>> <- rest,
         {rhs, rest} <- Integer.parse(rest),
         <<?), rest::binary>> <- rest do
      [{:mul, lhs, rhs} | parse(rest)]
    else
      <<_::utf8, rest::binary>> -> parse(rest)
    end
  end
end

defmodule AocEx.Y2024.D3 do
  use AocEx.Boilerplate,
    file: "priv/y2024/d03.txt",
    transform: &__MODULE__.Parser.parse/1

  def part1(input \\ processed()) do
    input
    |> eval1()
    |> Enum.sum()
  end

  def part2(input \\ processed()) do
    input
    |> eval2(true)
    |> Enum.sum()
  end

  defp eval1([{:mul, lhs, rhs} | es]), do: [lhs * rhs | eval1(es)]
  defp eval1([do? | es]) when do? in [:do, :dont], do: eval1(es)
  defp eval1([]), do: []

  defp eval2([{:mul, lhs, rhs} | es], true), do: [lhs * rhs | eval2(es, true)]
  defp eval2([{:mul, _, _} | es], false), do: eval2(es, false)
  defp eval2([:do|es], _), do: eval2(es, true)
  defp eval2([:dont|es], _), do: eval2(es, false)
  defp eval2([], _), do: []
end
