defmodule AocEx.Y2024.D1 do
  use AocEx.Boilerplate,
    file: "priv/y2024/d01.txt",
    transform: fn raw ->
      raw
      |> String.split("\n")
      |> Enum.reject(&(&1 == ""))
      |> Enum.map(fn line ->
        line
        |> String.split("   ")
        |> Enum.map(&Integer.parse/1)
        |> Enum.map(&elem(&1, 0))
      end)
      |> Enum.map(&List.to_tuple/1)
      |> Enum.unzip()
    end

  def part1({left, right} \\ processed()) do
    left = Enum.sort(left)
    right = Enum.sort(right)

    Enum.zip(left, right)
    |> Enum.map(fn {l, r} -> abs(l - r) end)
    |> Enum.sum()
  end

  def part2({left, right} \\ processed()) do
    frequencies = Enum.frequencies(right)

    left
    |> Enum.map(& &1 * Map.get(frequencies, &1, 0))
    |> Enum.sum()
  end
end
