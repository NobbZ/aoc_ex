defmodule AocEx.Y2024.D2 do
  use AocEx.Boilerplate,
    file: "priv/y2024/d02.txt",
    transform: fn raw ->
      raw
      |> String.split("\n")
      |> Enum.reject(&(&1 == ""))
      |> Enum.map(fn line ->
        line
        |> String.split(" ")
        |> Enum.map(&Integer.parse/1)
        |> Enum.map(&elem(&1, 0))
      end)
    end

  def part1(reports \\ processed()) do
    Enum.count(reports, &safe?/1)
  end

  def part2(reports \\ processed()) do
    Enum.count(reports, &dampened_safe?/1)
  end

  defp safe?([l, l | _]), do: false
  defp safe?(report = [l1, l2 | _]), do: safe?(report, sign(l1 - l2))

  defp safe?([_], _), do: true

  defp safe?([l1, l2 | rest], s) do
    if abs(l1 - l2) <= 3 and sign(l1 - l2) == s do
      safe?([l2 | rest], s)
    else
      false
    end
  end

  defp dampened_safe?(report), do: safe?(report) || dampened_safe?(report, [])

  defp dampened_safe?([h | rest], prev) do
    this =
      prev
      |> Enum.reverse()
      |> Enum.concat(rest)
      |> safe?()

    this || dampened_safe?(rest, [h | prev])
  end

  defp dampened_safe?([], _), do: false

  defp sign(n) when n == 0, do: 0
  defp sign(n) when n < 0, do: -1
  defp sign(n) when n > 0, do: 1
end
