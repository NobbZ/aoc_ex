defmodule AocEx.Y2020.D6 do
  use AocEx.Boilerplate,
    file: "priv/y2020/d06.txt",
    transform: fn raw ->
      raw
      |> String.split("\n\n", triem: true)
      |> Enum.map(fn group ->
        group
        |> String.split("\n", trim: true)
        |> Enum.map(&to_charlist/1)
        |> Enum.map(fn cs -> Enum.map(cs, &(&1 - ?a)) end)
        |> Enum.map(&MapSet.new/1)
      end)
    end

  def part1(input \\ processed()) do
    input
    |> Enum.map(mk_mapper(&MapSet.union/2))
    |> Enum.sum()
  end

  def part2(input \\ processed()) do
    input
    |> Enum.map(mk_mapper(&MapSet.intersection/2))
    |> Enum.sum()
  end

  defp mk_mapper(f) do
    fn [h | t] -> t |> Enum.reduce(h, f) |> Enum.count() end
  end
end
