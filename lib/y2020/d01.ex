defmodule AocEx.Y2020.D1 do
  use AocEx.Boilerplate,
    file: "priv/y2020/d01.txt",
    transform: fn raw -> raw |> String.split() |> Enum.map(&String.to_integer/1) end

  def part1(input \\ processed(), target \\ 2020) do
    smaller =
      input
      |> split_for(target)
      |> search_for(target)

    bigger = target - smaller

    bigger * smaller
  end

  def part2(input \\ processed()) do
    {n, m, o} =
      input
      |> tails()
      |> search3()

    n * m * o
  end

  defp search_for({small, big}, target), do: search_for(small, big, target)

  defp search_for(small, big, target) do
    Enum.find(small, fn n -> Enum.any?(big, &(n + &1 == target)) end)
  end

  defp tails([]), do: []
  defp tails([h | t]), do: [[h | t] | tails(t)]

  defp search3([]), do: []

  defp search3([[pivot | rest] | t]) do
    barrier = 2020 - pivot
    {small, big} = split_for(rest, barrier)

    case search_for(small, big, barrier) do
      nil -> search3(t)
      n -> {pivot, n, barrier - n}
    end
  end

  defp split_for(enumerable, target) do
    middle = div(target, 2)
    Enum.split_with(enumerable, fn n -> n < middle end)
  end
end
