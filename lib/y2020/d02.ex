defmodule AocEx.Y2020.D2 do
  use AocEx.Boilerplate,
    file: "priv/y2020/d02.txt",
    transform: fn raw ->
      raw
      |> String.split("\n")
      |> Enum.reject(&(&1 == ""))
      |> Enum.map(fn line ->
        [r, <<c::utf8, ":">>, pw] = line |> String.split()
        [l, r] = String.split(r, "-") |> Enum.map(&String.to_integer/1)

        %{range: l..r, letter: c, password: pw |> String.to_charlist()}
      end)
    end

  @type t :: %{range: Range.t(), letter: char(), password: charlist()}

  @spec part1([t]) :: non_neg_integer()
  def part1(input \\ processed()) do
    input
    |> Enum.map(fn %{password: pw} = data -> Map.put(data, :freq, Enum.frequencies(pw)) end)
    |> Enum.count(fn %{freq: freq, letter: l, range: r} -> freq[l] in r end)
  end

  def part2(input \\ processed()) do
    input
    |> Enum.map(fn %{password: pw, range: r} = data ->
      Map.put(data, :check, {Enum.at(pw, r.first - 1), Enum.at(pw, r.last - 1)})
    end)
    |> Enum.count(fn
      %{check: {l, l}, letter: l} -> false
      %{check: {a, b}, letter: l} -> l in [a, b]
    end)
  end
end
