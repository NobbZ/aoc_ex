defmodule AocEx.Y2020.D4 do
  use AocEx.Boilerplate,
    file: "priv/y2020/d04.txt",
    transform: fn raw ->
      raw
      |> String.split("\n\n")
      |> Enum.map(fn pp ->
        pp |> String.split() |> Map.new(&(&1 |> String.split(":") |> List.to_tuple()))
      end)
    end

  @required_fields ~W[byr iyr eyr hgt hcl ecl pid]

  def part1(input \\ processed()) do
    input
    |> Enum.count(fn pp -> Enum.all?(@required_fields, &Map.has_key?(pp, &1)) end)
  end

  def part2(input \\ processed()) do
    input
    |> Enum.count(fn pp ->
      Enum.all?(@required_fields, &Map.has_key?(pp, &1)) && Enum.all?(pp, &validate/1)
    end)
  end

  def validate({"byr", {byr, ""}}), do: byr in 1920..2002
  def validate({"byr", byr}), do: validate({"byr", Integer.parse(byr)})
  def validate({"iyr", {iyr, ""}}), do: iyr in 2010..2020
  def validate({"iyr", iyr}), do: validate({"iyr", Integer.parse(iyr)})
  def validate({"eyr", {eyr, ""}}), do: eyr in 2020..2030
  def validate({"eyr", eyr}), do: validate({"eyr", Integer.parse(eyr)})
  def validate({"hgt", {hgt, "cm"}}), do: hgt in 150..193
  def validate({"hgt", {hgt, "in"}}), do: hgt in 59..79
  def validate({"hgt", hgt}) when is_tuple(hgt) or is_atom(hgt), do: false
  def validate({"hgt", hgt}), do: validate({"hgt", Integer.parse(hgt)})
  def validate({"hcl", hcl}), do: hcl =~ ~R/^#[a-f0-9]{6}$/
  def validate({"ecl", ecl}), do: ecl in ~W[amb blu brn gry grn hzl oth]
  def validate({"pid", pid}), do: pid =~ ~R/^[0-9]{9}$/
  def validate({"cid", _}), do: true
end
