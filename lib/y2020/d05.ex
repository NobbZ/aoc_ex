defmodule AocEx.Y2020.D5 do
  use AocEx.Boilerplate,
    file: "priv/y2020/d05.txt",
    transform: fn raw ->
      raw
      |> String.split("\n", trim: true)
      |> Enum.map(fn code ->
        {row, col} = String.split_at(code, 7)

        {row, ""} =
          row |> String.replace("B", "1") |> String.replace("F", "0") |> Integer.parse(2)

        {col, ""} =
          col |> String.replace("R", "1") |> String.replace("L", "0") |> Integer.parse(2)

        {row, col}
      end)
    end

  def part1(input \\ processed()) do
    input
    |> Enum.map(fn {row, col} -> row * 8 + col end)
    |> Enum.max()
  end

  def part2(input \\ processed()) do
    input
    |> Enum.map(fn {row, col} -> row * 8 + col end)
    |> Enum.sort()
    |> Enum.reduce_while(nil, fn
      id, nil -> {:cont, id}
      id, old_id when old_id + 1 == id -> {:cont, id}
      _, old_id -> {:halt, old_id + 1}
    end)
  end
end
