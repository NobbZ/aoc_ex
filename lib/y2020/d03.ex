defmodule AocEx.Y2020.D3 do
  use AocEx.Boilerplate,
    transform: fn raw ->
      lines =
        raw
        |> String.split("\n", trim: true)

      line_count = Enum.count(lines)
      row_count = lines |> Enum.map(&String.length/1) |> Enum.max()

      board =
        lines
        |> Enum.with_index()
        |> Enum.map(fn {line, idx} ->
          line
          |> String.graphemes()
          |> Enum.map(fn
            "." -> nil
            "#" -> :tree
          end)
          |> Enum.with_index()
          |> Enum.map(fn {field, x} -> {{x, idx}, field} end)
          |> Enum.reject(fn {_, f} -> is_nil(f) end)
        end)
        |> List.flatten()
        |> Map.new()

      %{board: board, width: row_count, height: line_count}
    end

  def part1(input \\ processed()) do
    input
    |> step(3, 1)
    |> Enum.count(&(not is_nil(&1)))
  end

  def part2(input \\ processed()) do
    [{1, 1}, {3, 1}, {5, 1}, {7, 1}, {1, 2}]
    |> Task.async_stream(fn {r, d} ->
      input
      |> step(r, d)
      |> Enum.count(&(not is_nil(&1)))
    end)
    |> Enum.reduce(1, fn {:ok, n}, prod -> n * prod end)
  end

  defp step(tree_map, right, down, coords \\ {0, 0})
  defp step(%{height: h}, _, _, {_, y}) when h <= y, do: []

  defp step(%{board: b, width: w} = tree_map, r, d, {x, y}) do
    [b[{x, y}] | step(tree_map, r, d, {rem(x + r, w), y + d})]
  end
end
