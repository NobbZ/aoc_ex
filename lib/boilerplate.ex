defmodule AocEx.Boilerplate do
  defstruct [:raw, :file, :transform, :part1, :part2]

  defmacro __using__(opts), do: using(opts, __CALLER__)

  @spec using(Keyword.t(), %__MODULE__{}, Macro.Env.t()) :: Macro.t()
  defp using(list_of_opts, opts \\ %__MODULE__{}, caller)
  defp using([{:raw, raw} | t], opts, caller), do: using(t, %{opts | raw: raw}, caller)
  defp using([{:file, file} | t], opts, caller), do: using(t, %{opts | file: file}, caller)
  defp using([{:part1, part1} | t], opts, caller), do: using(t, %{opts | part1: part1}, caller)
  defp using([{:part2, part2} | t], opts, caller), do: using(t, %{opts | part2: part2}, caller)

  defp using([{:transform, transform} | t], opts, caller),
    do: using(t, %{opts | transform: transform}, caller)

  defp using([], opts, caller) do
    {file, raw, part1, part2, transformer} = get_opts(opts, caller)

    quote do
      @external_resource unquote(file)
      @__processed__ unquote(raw) |> String.trim() |> unquote(transformer).()

      def processed(), do: @__processed__

      def transform(input), do: unquote(transformer).(input)

      def main(opts) do
        [part1: unquote(part1), part2: unquote(part2)]
        |> Enum.each(fn {n, f} ->
          {t, v} = Timex.Duration.measure(f)

          IO.puts("#{__MODULE__}, #{n} => #{Aoc.Duration.to_string(t)}; <#{:erlang.phash2(v)}>")

          if opts[:spoil], do: IO.puts("#{unquote(caller.module)}, #{n} -> #{v}")
        end)
      end
    end
  end

  @spec build_filename(Macro.Env.t()) :: String.t()
  defp build_filename(caller) do
    lib_folder = Path.join(File.cwd!(), "lib")

    path_suffix =
      caller.file
      |> String.replace_suffix(".ex", ".txt")
      |> Path.relative_to(lib_folder)

    :aoc_ex
    |> :code.priv_dir()
    |> Path.join(path_suffix)
  end

  defp get_opts(opts, caller) do
    file = opts.file || build_filename(caller)

    raw = opts.raw || File.read!(file)

    part1 = opts.part1 || quote do: &part1/0
    part2 = opts.part2 || quote do: &part2/0

    transformer = opts.transform || quote do: fn a -> a end

    {file, raw, part1, part2, transformer}
  end
end
