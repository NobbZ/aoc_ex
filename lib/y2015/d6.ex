defmodule AocEx.Y2015.D6 do
  use AocEx.Boilerplate,
    file: "priv/y2015/d6.txt",
    transform: fn raw ->
      raw
      |> String.trim()
      |> String.split("\n")
      |> Stream.map(fn
        "turn on " <> tail -> {:on, tail}
        "turn off " <> tail -> {:off, tail}
        "toggle " <> tail -> {:tgl, tail}
      end)
      |> Stream.map(fn {cmd, area} ->
        [x1, y1, x2, y2] =
          area
          |> String.split(" through ")
          |> Enum.map(&String.split(&1, ","))
          |> List.flatten()
          |> Enum.map(&String.to_integer/1)

        {cmd, {{x1, y1}, {x2, y2}}}
      end)
      |> Enum.into([])
    end

  def part1(input \\ processed()) do
    input
    |> Stream.flat_map(fn {cmd, area} ->
      [cmd] |> Stream.cycle() |> Enum.zip(area_to_list(area))
    end)
    |> Enum.reduce(MapSet.new(), fn
      {:on, coord}, lights ->
        MapSet.put(lights, coord)

      {:off, coord}, lights ->
        MapSet.delete(lights, coord)

      {:tgl, coord}, lights ->
        if MapSet.member?(lights, coord),
          do: MapSet.delete(lights, coord),
          else: MapSet.put(lights, coord)
    end)
    |> MapSet.size()
  end

  def part2(input \\ processed()) do
    input
    |> Stream.flat_map(fn {cmd, area} ->
      [cmd] |> Stream.cycle() |> Enum.zip(area_to_list(area))
    end)
    |> Enum.reduce(%{}, fn
      {:on, coord}, lights -> Map.update(lights, coord, 1, &(&1 + 1))
      {:off, coord}, lights -> Map.update(lights, coord, 0, &max(0, &1 - 1))
      {:tgl, coord}, lights -> Map.update(lights, coord, 2, &(&1 + 2))
    end)
    |> Map.values()
    |> Enum.sum()
  end

  defp area_to_list({{x1, y1}, {x2, y2}}), do: for(x <- x1..x2, y <- y1..y2, do: {x, y})
end
