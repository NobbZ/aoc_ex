defmodule AocEx.Y2015.D3 do
  use AocEx.Boilerplate,
    file: "priv/y2015/d3.txt",
    transform: fn raw ->
      raw
      |> String.trim()
      |> String.graphemes()
      |> Enum.map(fn
        "^" -> :up
        ">" -> :right
        "v" -> :down
        "<" -> :left
      end)
    end

  def part1(input \\ processed()) do
    input
    |> move_around()
    |> MapSet.size()
  end

  def part2(input \\ processed()) do
    input
    |> move_around_with_robot()
    |> MapSet.size()
  end

  defp move_around(input), do: move_around(input, {0, 0}, MapSet.new([{0, 0}]))

  defp move_around([], _, visited), do: visited

  defp move_around([d | dirs], santa, visited) do
    new_santa = move_one(santa, d)
    new_visited = MapSet.put(visited, new_santa)
    move_around(dirs, new_santa, new_visited)
  end

  defp move_around_with_robot(input) do
    move_around_with_robot(input, {0, 0}, {0, 0}, MapSet.new([{0, 0}]))
  end

  defp move_around_with_robot([], _, _, visited), do: visited

  defp move_around_with_robot([ds, dr | dirs], santa, robot, visited) do
    new_santa = move_one(santa, ds)
    new_robot = move_one(robot, dr)
    new_visited = visited |> MapSet.put(new_santa) |> MapSet.put(new_robot)
    move_around_with_robot(dirs, new_santa, new_robot, new_visited)
  end

  defp move_one({x, y}, :up), do: {x, y + 1}
  defp move_one({x, y}, :right), do: {x + 1, y}
  defp move_one({x, y}, :down), do: {x, y - 1}
  defp move_one({x, y}, :left), do: {x - 1, y}
end
