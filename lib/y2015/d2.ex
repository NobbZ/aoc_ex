defmodule AocEx.Y2015.D2 do
  use AocEx.Boilerplate,
    file: "priv/y2015/d2.txt",
    transform: fn raw ->
      raw
      |> String.split()
      |> Enum.map(fn box ->
        box
        |> String.split("x")
        |> Enum.map(&String.to_integer/1)
      end)
      |> Enum.map(fn [l, w, h] -> %{l: l, w: w, h: h} end)
    end

  def part1(input \\ processed()) do
    input
    |> Enum.map(fn box ->
      squares = [box.l * box.w, box.w * box.h, box.h * box.l]

      Enum.sum([Enum.min(squares) | Enum.map(squares, &(&1 * 2))])
    end)
    |> Enum.sum()
  end

  def part2(input \\ processed()) do
    input
    |> Enum.map(fn box ->
      sides = box |> Map.values() |> Enum.sort()

      wrap = sides |> Enum.take(2) |> Enum.map(&(&1 * 2)) |> Enum.sum()
      bow = Enum.reduce(sides, 1, &(&1 * &2))

      wrap + bow
    end)
    |> Enum.sum()
  end
end
