defmodule AocEx.Y2015.D4 do
  use AocEx.Boilerplate,
    raw: "yzbqklnj"

  def part1(input \\ processed()) do
    find_hash_id(input, &five_leading/1)
  end

  def part2(input \\ processed()) do
    find_hash_id(input, &six_leading/1)
  end

  defp find_hash_id(prefix, predicate), do: find_hash_id(prefix, predicate, 0)

  defp find_hash_id(prefix, p, n) do
    (prefix <> to_string(n))
    |> :erlang.md5()
    |> p.()
    |> if(do: n, else: find_hash_id(prefix, p, n + 1))
  end

  defp five_leading(<<0::size(5)-unit(4), _::size(27)-unit(4)>>), do: true
  defp five_leading(_), do: false

  defp six_leading(<<0::size(6)-unit(4), _::size(26)-unit(4)>>), do: true
  defp six_leading(_), do: false
end
