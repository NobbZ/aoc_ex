defmodule AocEx.Y2015.D5 do
  use AocEx.Boilerplate,
    file: "priv/y2015/d5.txt",
    transform: fn raw ->
      raw |> String.trim() |> String.split()
    end

  @vowels ~w[a e i o u]
  @bad_pairs ~w[ab cd pq xy] |> Enum.map(&String.graphemes/1) |> Enum.map(&List.to_tuple/1)

  def part1(input \\ processed()) do
    input
    |> Stream.map(&String.graphemes/1)
    # 1. Rule: At least 3 vowels
    |> Stream.filter(&(vowels(&1) >= 3))
    # 2. Rule: At least one letter has to be twice in a row
    |> Stream.filter(&(Enum.dedup(&1) != &1))
    # 3. Rule: Has none of "ab", "cd", "pq" or "xy"
    |> Stream.filter(fn word = [_ | tl] ->
      pairs = Enum.zip(word, tl)
      Enum.all?(@bad_pairs, &(&1 not in pairs))
    end)
    |> Enum.count()
  end

  def part2(input \\ processed()) do
    input
    |> Stream.map(&String.graphemes/1)
    # 1. Rule: Has any pair twice (non-overlapping)
    |> Stream.filter(&any_pair_twice?/1)
    # 2. Rule: Has one letter repeated with another inbetween
    |> Stream.filter(&any_repeated_with_another_inbetween?/1)
    |> Enum.count()
  end

  defp vowels(list) do
    Enum.reduce(list, 0, fn
      vowel, cnt when vowel in @vowels -> cnt + 1
      _, cnt -> cnt
    end)
  end

  defp any_pair_twice?([]), do: false
  defp any_pair_twice?([_]), do: false

  defp any_pair_twice?([a, b | t]) do
    {a, b}
    |> find_again(t)
    |> if(do: true, else: any_pair_twice?([b | t]))
  end

  defp any_repeated_with_another_inbetween?([]), do: false
  defp any_repeated_with_another_inbetween?([_]), do: false
  defp any_repeated_with_another_inbetween?([_, _]), do: false
  defp any_repeated_with_another_inbetween?([x, _, x | _]), do: true
  defp any_repeated_with_another_inbetween?([_ | t]), do: any_repeated_with_another_inbetween?(t)

  defp find_again(_, []), do: false
  defp find_again({a, b}, [a, b | _]), do: true
  defp find_again(pair, [_ | t]), do: find_again(pair, t)
end
