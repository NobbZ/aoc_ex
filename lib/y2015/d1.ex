defmodule AocEx.Y2015.D1 do
  use AocEx.Boilerplate, file: "priv/y2015/d1.txt"

  def part1(input \\ processed()) do
    input
    |> diff_count_parens()
  end

  def part2(input \\ processed()) do
    input
    |> find_floor(-1)
  end

  defp diff_count_parens(input, acc \\ 0)
  defp diff_count_parens("", acc), do: acc
  defp diff_count_parens("(" <> t, acc), do: diff_count_parens(t, acc + 1)
  defp diff_count_parens(")" <> t, acc), do: diff_count_parens(t, acc - 1)

  defp find_floor(input, target), do: find_floor(input, target, 0, 0)
  defp find_floor(_input, target, idx, target), do: idx

  defp find_floor("(" <> input, target, idx, current),
    do: find_floor(input, target, idx + 1, current + 1)

  defp find_floor(")" <> input, target, idx, current),
    do: find_floor(input, target, idx + 1, current - 1)
end
