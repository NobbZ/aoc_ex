defmodule AocEx.Y2019.D6 do
  use AocEx.Boilerplate,
    file: "priv/y2019/d6.txt",
    transform: fn raw ->
      raw
      |> String.split()
      |> Enum.map(&String.split(&1, ")"))
      |> Enum.map(&List.to_tuple/1)
      |> Enum.group_by(&elem(&1, 0), &elem(&1, 1))
    end

  def part1(input \\ processed()) do
    input
    |> calc_distances("COM")
    |> Enum.reduce(0, fn {_, oc}, occ -> oc + occ end)
  end

  def part2(input \\ processed()) do
    path_length =
      input
      |> load_into_digraph()
      |> get_shortest_path("YOU", "SAN")
      |> Enum.count()

    # The length is in vertices, so we need to:
    # - subtract 1 to adjust to the number of nodes passed
    # - subtract 2 to get rid of the "from" and "to"
    path_length - 2 - 1
  end

  defp calc_distances(orbit_map, current),
    do: calc_distances(orbit_map, current, 0, %{current => 0})

  defp calc_distances(om, curr, dist, distances) do
    distances = Map.put(distances, curr, dist)

    case om[curr] do
      nil ->
        distances

      children ->
        Enum.reduce(children, distances, fn child, distances ->
          calc_distances(om, child, dist + 1, distances)
        end)
    end
  end

  defp load_into_digraph(map) do
    dcg = :digraph.new([:cyclic, :private])

    vertices =
      List.flatten(Map.keys(map) ++ Map.values(map))
      |> MapSet.new()
      |> Map.new(fn vert -> {vert, :digraph.add_vertex(dcg)} end)

    Enum.each(map, fn {orig, orbitters} ->
      Enum.each(orbitters, fn orbitter ->
        :digraph.add_edge(dcg, vertices[orbitter], vertices[orig])
        :digraph.add_edge(dcg, vertices[orig], vertices[orbitter])
      end)
    end)

    {dcg, vertices}
  end

  defp get_shortest_path({dcg, vertices}, from, to) do
    from = vertices[from]
    to = vertices[to]

    :digraph.get_short_path(dcg, from, to)
  end
end
