defmodule AocEx.Y2019.D3 do
  use AocEx.Boilerplate,
    file: "priv/y2019/d3.txt",
    transform: fn raw ->
      raw
      |> String.split()
      |> Enum.map(fn line ->
        line
        |> String.split(",")
        |> Enum.map(fn
          "U" <> num -> {:up, String.to_integer(num)}
          "R" <> num -> {:right, String.to_integer(num)}
          "D" <> num -> {:down, String.to_integer(num)}
          "L" <> num -> {:left, String.to_integer(num)}
        end)
      end)
    end

  def part1(input \\ processed()) do
    [wire1, wire2] =
      input
      |> Enum.map(&connect/1)
      |> Enum.map(&MapSet.new/1)

    MapSet.intersection(wire1, wire2)
    |> MapSet.delete({0, 0})
    |> Enum.map(fn {x, y} -> abs(x) + abs(y) end)
    |> Enum.min()
  end

  def part2(input \\ processed()) do
    [path1, path2] = Enum.map(input, &connect/1)

    coords1 = MapSet.new(path1)
    coords2 = MapSet.new(path2)

    MapSet.intersection(coords1, coords2)
    |> MapSet.delete({0, 0})
    |> Enum.map(fn coord ->
      count1 = Enum.find_index(path1, fn pos -> pos == coord end)
      count2 = Enum.find_index(path2, fn pos -> pos == coord end)
      count1 + count2
    end)
    |> Enum.min()
  end

  defp connect(input, acc \\ [{0, 0}])
  defp connect([], acc), do: Enum.reverse(acc)
  defp connect([{_, 0} | t], acc), do: connect(t, acc)

  defp connect([{dir, n} | t], [{x, y} | _] = acc) do
    n = n - 1

    pos =
      case dir do
        :up -> {x, y + 1}
        :right -> {x + 1, y}
        :down -> {x, y - 1}
        :left -> {x - 1, y}
      end

    connect([{dir, n} | t], [pos | acc])
  end
end
