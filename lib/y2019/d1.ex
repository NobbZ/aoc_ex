defmodule AocEx.Y2019.D1 do
  use AocEx.Boilerplate,
    file: "priv/y2019/d1.txt",
    transform: fn raw -> raw |> String.split() |> Enum.map(&String.to_integer(&1)) end

  def part1(input \\ processed()) do
    input
    |> Stream.map(&calc_fuel/1)
    |> Enum.sum()
  end

  def part2(input \\ processed()) do
    input
    |> Stream.map(fn m -> m |> calc_fuel() |> add_fuel() end)
    |> Enum.sum()
  end

  defp calc_fuel(mass), do: div(mass, 3) - 2

  defp add_fuel(mass), do: add_fuel(mass, mass)

  defp add_fuel(mass, sum) do
    new_mass = calc_fuel(mass)

    case new_mass do
      m when m <= 0 -> sum
      m -> add_fuel(m, sum + m)
    end
  end
end
