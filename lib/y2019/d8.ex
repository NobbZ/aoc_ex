defmodule AocEx.Y2019.D8 do
  use AocEx.Boilerplate,
    file: "priv/y2019/d8.txt",
    transform: fn raw ->
      raw
      |> String.trim()
      |> String.graphemes()
      |> Enum.map(&String.to_integer/1)
    end

  @width 25
  @height 6

  def part1(input \\ processed()) do
    %{1 => ones, 2 => twos} =
      input
      |> Enum.chunk_every(@width * @height)
      |> Enum.min_by(fn layer -> layer |> Enum.filter(&(&1 == 0)) |> Enum.count() end)
      |> Enum.reduce(%{1 => 0, 2 => 0}, fn digit, acc -> Map.update(acc, digit, 0, &(&1 + 1)) end)

    ones * twos
  end

  def part2(input \\ processed()) do
    input
    |> Enum.chunk_every(@width * @height)
    |> Enum.zip()
    |> Enum.map(fn pixel -> pixel |> Tuple.to_list() |> Enum.find(&(&1 != 2)) end)
    |> Enum.map(fn
      0 -> " "
      1 -> "*"
    end)
    |> Enum.chunk_every(@width)
    |> Enum.map_join("\n", &Enum.join/1)
  end
end
