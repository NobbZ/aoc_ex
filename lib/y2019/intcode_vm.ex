defmodule AocEx.Y2019.IntcodeVM do
  defstruct mem: nil, ic: 0, state: :unitialized, rel_base: 0, input: [], output: []

  @type modes :: list()
  @type state :: :unitialized | :ready | :running | :waiting | :halted
  @type t :: %__MODULE__{
          mem: :array.array(),
          ic: non_neg_integer(),
          state: state(),
          rel_base: non_neg_integer(),
          input: list(integer()),
          output: list(integer())
        }

  @spec new(Enum.t()) :: t
  def new(enumerable) do
    %__MODULE__{
      mem: enumerable |> Enum.into([]) |> :array.from_list(0),
      state: :ready
    }
  end

  @spec exec(t) :: t
  def exec(vm) do
    vm = %{vm | state: :running}
    {opcode, modes} = vm |> peek(vm.ic) |> parse_intcode()

    vm
    |> dispatch(opcode, modes)
    |> case do
      %__MODULE__{state: :running} = vm -> exec(vm)
      vm -> vm
    end
  end

  defp dispatch(vm, opcode, modes)
  defp dispatch(vm, :add, modes), do: apply_with_writeback(vm, &+/2, modes)
  defp dispatch(vm, :mul, modes), do: apply_with_writeback(vm, &*/2, modes)
  defp dispatch(vm, :eq, modes), do: apply_with_writeback(vm, pred(:eq), modes)
  defp dispatch(vm, :lt, modes), do: apply_with_writeback(vm, pred(:lt), modes)
  defp dispatch(vm, :jmpnz, modes), do: cond_jmp(vm, &(&1 != 0), modes)
  defp dispatch(vm, :jmpz, modes), do: cond_jmp(vm, &(&1 == 0), modes)
  defp dispatch(vm, :input, modes), do: read_input(vm, modes)
  defp dispatch(vm, :output, modes), do: write_output(vm, modes)
  defp dispatch(vm, :adj_rb, modes), do: adj_rb(vm, modes)
  defp dispatch(vm, :stop, []), do: %{vm | state: :halted}

  defp pred(:eq), do: pred(&==/2)
  defp pred(:lt), do: pred(&</2)
  defp pred(p) when is_function(p, 2), do: &if(p.(&1, &2), do: 1, else: 0)

  @spec peek(t, non_neg_integer()) :: non_neg_integer()
  def peek(vm, addr) do
    :array.get(addr, vm.mem)
  catch
    _, _ ->
      IO.puts("#{addr} can't be accessed in VM")
      raise ArgumentError
  end

  @spec poke(t, non_neg_integer(), integer()) :: t
  def poke(vm, addr, value), do: %{vm | mem: :array.set(addr, value, vm.mem)}

  @spec push_input(t, integer()) :: t()
  # TODO: probably use a dequeue here instead of list
  def push_input(vm, value), do: %{vm | input: vm.input ++ [value]}

  @spec output_to_list(t) :: {t, list(integer())}
  def output_to_list(%{output: output} = vm), do: {%{vm | output: []}, Enum.reverse(output)}

  ###############################################################

  defp apply_with_writeback(vm, f, modes) do
    {args, [target]} = vm |> arg_spec(modes) |> Enum.split(-1)
    target = resolve(vm, target)
    result = apply(f, Enum.map(args, &deref(vm, &1)))

    vm |> inc_ic(2 + Enum.count(args)) |> poke(target, result)
  end

  defp cond_jmp(vm, p, modes) do
    {args, [jmp_to]} = vm |> arg_spec(modes) |> Enum.split(-1)
    args = Enum.map(args, &deref(vm, &1))

    ic = if apply(p, args), do: deref(vm, jmp_to), else: inc_ic(vm, 1 + Enum.count(modes)).ic

    set_ic(vm, ic)
  end

  defp read_input(%__MODULE__{input: []} = vm, _), do: %{vm | state: :waiting}

  defp read_input(vm, mode) do
    [spec] = arg_spec(vm, mode)
    {vm, input} = pop_input(vm)
    vm |> inc_ic(2) |> poke(resolve(vm, spec), input)
  end

  defp write_output(vm, mode) do
    [spec] = arg_spec(vm, mode)
    inc_ic(%{vm | output: [deref(vm, spec) | vm.output]}, 2)
  end

  defp adj_rb(vm, mode) do
    [spec] = arg_spec(vm, mode)
    inc_ic(%{vm | rel_base: vm.rel_base + deref(vm, spec)}, 2)
  end

  ###############################################################

  defp inc_ic(vm, inc), do: set_ic(vm, vm.ic + inc)

  defp set_ic(vm, ic), do: %{vm | ic: ic}

  defp resolve(vm, {:rel, addr}), do: vm.rel_base + peek(vm, addr)
  defp resolve(vm, {:pos, addr}), do: peek(vm, addr)

  defp deref(vm, {:imm, addr}), do: peek(vm, addr)
  defp deref(vm, spec), do: peek(vm, resolve(vm, spec))

  defp pop_input(vm), do: {%{vm | input: tl(vm.input)}, hd(vm.input)}

  defp arg_spec(vm, modes) do
    modes |> Enum.with_index(1) |> Enum.map(fn {m, o} -> {m, vm.ic + o} end)
  end

  defp parse_intcode(intcode) do
    {opcode, arity} = to_opcode(rem(intcode, 100))
    {opcode, to_modes(div(intcode, 100), arity)}
  end

  defp to_modes(n, arity, acc \\ [])
  defp to_modes(_, 0, acc), do: acc

  defp to_modes(n, arity, acc) do
    digit = n |> div(pow(10, arity - 1)) |> rem(10)
    to_modes(n, arity - 1, [access_mode(digit) | acc])
  end

  defp pow(_, 0), do: 1
  defp pow(n, m), do: n * pow(n, m - 1)

  ###############################################################

  %{
    # $1 + $2 -> $3
    1 => {:add, 3},
    # $1 + $2 -> $3
    2 => {:mul, 3},
    # pop(IN) -> $1
    3 => {:input, 1},
    # push($1, OUT)
    4 => {:output, 1},
    # if $1 is non-zero -> ic = $2
    5 => {:jmpnz, 2},
    # if $1 is zero -> ic = $2
    6 => {:jmpz, 2},
    # if $1 < $2 then 1 -> $3 else 0 -> $3
    7 => {:lt, 3},
    # if $1 == $2 then 1 -> $3 else 0 -> $3
    8 => {:eq, 3},
    # adds $1 to the rel_base
    9 => {:adj_rb, 1},
    # stop execution of the VM
    99 => {:stop, 0}
  }
  |> Enum.each(fn {intcode, opcode} ->
    defp to_opcode(unquote(intcode)), do: unquote(opcode)
  end)

  %{0 => :pos, 1 => :imm, 2 => :rel}
  |> Enum.each(fn {digit, am} ->
    defp access_mode(unquote(digit)), do: unquote(am)
  end)
end
