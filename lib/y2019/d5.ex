defmodule AocEx.Y2019.D5 do
  use AocEx.Boilerplate,
    file: "priv/y2019/d5.txt",
    transform: fn raw ->
      raw
      |> String.split(",")
      |> Enum.map(&String.to_integer/1)
      |> AocEx.Y2019.IntcodeVM.new()
    end

  alias AocEx.Y2019.IntcodeVM

  def part1(input \\ processed()) do
    input
    |> IntcodeVM.push_input(1)
    |> IntcodeVM.exec()
    |> IntcodeVM.output_to_list()
    |> elem(1)
    |> Enum.at(-1)
  end

  def part2(input \\ processed()) do
    input
    |> IntcodeVM.push_input(5)
    |> IntcodeVM.exec()
    |> IntcodeVM.output_to_list()
    |> elem(1)
    |> hd()
  end
end
