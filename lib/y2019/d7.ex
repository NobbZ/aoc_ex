defmodule AocEx.Y2019.D7 do
  use AocEx.Boilerplate,
    file: "priv/y2019/d7.txt",
    transform: fn raw ->
      raw
      |> String.split(",")
      |> Enum.map(&String.to_integer/1)
      |> AocEx.Y2019.IntcodeVM.new()
    end

  alias AocEx.Y2019.IntcodeVM

  def part1(input \\ processed()) do
    [0, 1, 2, 3, 4]
    |> permute
    |> Stream.map(fn phases ->
      Enum.reduce(phases, 0, &run_amplifier(&1, &2, input))
    end)
    |> Enum.max()
  end

  def part2(input \\ processed()) do
    [5, 6, 7, 8, 9]
    |> permute
    |> Stream.map(fn phases ->
      phases
      |> Enum.with_index()
      |> Enum.map(fn
        {phase, idx} -> {IntcodeVM.push_input(input, phase), idx}
      end)
      |> Enum.map(fn
        {vm, 0} -> {IntcodeVM.push_input(vm, 0), 0}
        item -> item
      end)
      |> run_loop()
      |> hd()
    end)
    |> Enum.max()
  end

  defp run_amplifier(phase, signal, vm) do
    vm
    |> IntcodeVM.push_input(phase)
    |> IntcodeVM.push_input(signal)
    |> IntcodeVM.exec()
    |> IntcodeVM.output_to_list()
    |> elem(1)
    |> hd
  end

  defp run_loop([{vm, idx}, {vm1, idx1} | t]) do
    vm
    |> IntcodeVM.exec()
    |> case do
      %{state: :waiting} = vm ->
        {vm, outputs} = IntcodeVM.output_to_list(vm)
        vm1 = Enum.reduce(outputs, vm1, &IntcodeVM.push_input(&2, &1))
        run_loop([{vm1, idx1} | t] ++ [{vm, idx}])

      %{state: :halted} = vm ->
        if idx == 4 do
          {_vm, outputs} = IntcodeVM.output_to_list(vm)
          outputs
        else
          {vm, outputs} = IntcodeVM.output_to_list(vm)
          vm1 = Enum.reduce(outputs, vm1, &IntcodeVM.push_input(&2, &1))
          run_loop([{vm1, idx1} | t] ++ [{vm, idx}])
        end
    end
  end

  defp permute([]), do: [[]]
  defp permute(list), do: for(elem <- list, rest <- permute(list -- [elem]), do: [elem | rest])
end
