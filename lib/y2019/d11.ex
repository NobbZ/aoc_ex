defmodule AocEx.Y2019.D11 do
  use AocEx.Boilerplate,
    file: "priv/y2019/d11.txt",
    transform: fn raw ->
      raw
      |> String.split(",")
      |> Enum.map(&String.to_integer/1)
      |> AocEx.Y2019.IntcodeVM.new()
    end

  alias AocEx.Y2019.IntcodeVM, as: VM

  def part1(vm \\ processed()) do
    vm
    |> test_paint_hull(:up, {0, 0}, %{})
    |> Enum.count()
  end

  def part2(vm \\ processed()) do
    vm
    |> test_paint_hull(:up, {0, 0}, %{{0, 0} => 1})
    |> pp()
  end

  defp test_paint_hull(%VM{} = vm, dir, pos, map) do
    vm
    |> VM.exec()
    |> case do
      %{state: :halted} ->
        map

      %{state: :waiting} = vm ->
        case VM.output_to_list(vm) do
          {vm, [color, turn]} ->
            dir = turn(turn, dir)
            map = Map.put(map, pos, color)
            pos = advance(pos, dir)
            test_paint_hull(vm, dir, pos, map)

          {vm, []} ->
            vm
            |> VM.push_input(Map.get(map, pos, 0))
            |> test_paint_hull(dir, pos, map)
        end
    end
  end

  defp pp(map) do
    white = map |> Enum.filter(fn {_, c} -> c == 1 end) |> Map.new()

    {xmin, ymin, xmax, ymax} =
      white
      |> Enum.reduce(nil, fn
        {{x, y}, _}, nil -> {x, y, x, y}
        {{x, _}, _}, {xmin, ymin, xmax, ymax} when x < xmin -> {x, ymin, xmax, ymax}
        {{x, _}, _}, {xmin, ymin, xmax, ymax} when x > xmax -> {xmin, ymin, x, ymax}
        {{_, y}, _}, {xmin, ymin, xmax, ymax} when y < ymin -> {xmin, y, xmax, ymax}
        {{_, y}, _}, {xmin, ymin, xmax, ymax} when y > ymax -> {xmin, ymin, xmax, y}
        _, bounds -> bounds
      end)

    pannels =
      for y <- ymax..ymin, x <- xmin..xmax do
        if white[{x, y}], do: "#", else: " "
      end

    "\n" <>
      (pannels
       |> Enum.chunk_every(1 + xmax - xmin)
       |> Enum.map_join("\n", &Enum.join/1))
  end

  defp turn(0, :up), do: :left
  defp turn(0, :right), do: :up
  defp turn(0, :down), do: :right
  defp turn(0, :left), do: :down
  defp turn(1, :up), do: :right
  defp turn(1, :right), do: :down
  defp turn(1, :down), do: :left
  defp turn(1, :left), do: :up

  defp advance({x, y}, :up), do: {x, y + 1}
  defp advance({x, y}, :right), do: {x + 1, y}
  defp advance({x, y}, :down), do: {x, y - 1}
  defp advance({x, y}, :left), do: {x - 1, y}
end
