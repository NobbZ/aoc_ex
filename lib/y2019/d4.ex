defmodule AocEx.Y2019.D4 do
  use AocEx.Boilerplate,
    file: "priv/y2019/d4.txt",
    raw: "284639-748759",
    transform: fn raw ->
      [lower, upper] = raw |> String.split("-") |> Enum.map(&String.to_integer/1)
      lower..upper
    end

  def part1(input \\ processed()) do
    input
    |> Stream.map(&Integer.digits/1)
    |> Stream.filter(&group_of_at_least_2?/1)
    |> Stream.filter(&digits_dont_decrease?/1)
    |> Enum.count()
  end

  def part2(input \\ processed()) do
    input
    |> Stream.map(&Integer.digits/1)
    # This rule is stronger than the group_of_at_least_2?/1 and can therefore replace it
    |> Stream.filter(&digits_dont_decrease?/1)
    |> Stream.filter(&group_of_exact_2?/1)
    |> Enum.count()
  end

  defp group_of_at_least_2?(digits), do: Enum.dedup(digits) != digits

  defp digits_dont_decrease?(digits) do
    digits
    |> Enum.reduce_while(0, fn
      curr, prev when curr >= prev -> {:cont, curr}
      _, _ -> {:halt, false}
    end)
  end

  defp group_of_exact_2?(digits) do
    group_lengths =
      digits
      |> Enum.reduce([], fn
        c, [[c | _] = grp | t] -> [[c | grp] | t]
        c, acc -> [[c] | acc]
      end)
      |> Enum.map(&Enum.count/1)

    2 in group_lengths
  end
end
