defmodule AocEx.Y2019.D2 do
  use AocEx.Boilerplate,
    file: "priv/y2019/d2.txt",
    transform: fn raw ->
      raw
      |> String.split(",")
      |> Enum.map(&String.to_integer/1)
      |> AocEx.Y2019.IntcodeVM.new()
    end

  alias AocEx.Y2019.IntcodeVM

  def part1(input \\ processed()) do
    input
    |> IntcodeVM.poke(1, 12)
    |> IntcodeVM.poke(2, 2)
    |> IntcodeVM.exec()
    |> IntcodeVM.peek(0)
  end

  def part2(input \\ processed()) do
    for noun <- 0..99, verb <- 0..99 do
      {noun, verb}
    end
    |> Stream.map(fn {noun, verb} ->
      {{noun, verb}, input |> IntcodeVM.poke(1, noun) |> IntcodeVM.poke(2, verb)}
    end)
    |> Stream.map(fn {params, vm} -> {params, IntcodeVM.exec(vm)} end)
    |> Stream.map(fn {params, vm} -> {params, IntcodeVM.peek(vm, 0)} end)
    |> Enum.find(fn {_, result} -> result == 19_690_720 end)
    |> case do
      {{noun, verb}, _result} -> 100 * noun + verb
    end
  end
end
