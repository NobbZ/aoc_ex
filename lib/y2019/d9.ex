defmodule AocEx.Y2019.D9 do
  use AocEx.Boilerplate,
    file: "priv/y2019/d9.txt",
    transform: fn raw ->
      raw
      |> String.split(",")
      |> Enum.map(&String.to_integer/1)
      |> AocEx.Y2019.IntcodeVM.new()
    end

  alias AocEx.Y2019.IntcodeVM

  def part1(input \\ processed()) do
    {_, [key_code]} =
      input |> IntcodeVM.push_input(1) |> IntcodeVM.exec() |> IntcodeVM.output_to_list()

    key_code
  end

  def part2(input \\ processed()) do
    {_, [coordinate]} =
      input |> IntcodeVM.push_input(2) |> IntcodeVM.exec() |> IntcodeVM.output_to_list()

    coordinate
  end
end
