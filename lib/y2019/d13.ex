defmodule AocEx.Y2019.D13 do
  use AocEx.Boilerplate,
    file: "priv/y2019/d13.txt",
    transform: fn raw ->
      raw
      |> String.split(",")
      |> Enum.map(&String.to_integer/1)
      |> AocEx.Y2019.IntcodeVM.new()
    end

  alias AocEx.Y2019.IntcodeVM, as: VM

  def part1(vm \\ processed()) do
    vm
    |> VM.exec()
    |> VM.output_to_list()
    |> elem(1)
    |> Enum.chunk_every(3)
    |> Enum.reduce(0, fn
      [_, _, 2], acc -> acc + 1
      _, acc -> acc
    end)
  end

  def part2(vm \\ processed()) do
    init = %{score: 0, bx: 0, px: 0, blocks: MapSet.new()}

    vm
    |> VM.poke(0, 2)
    |> play_game(init)
  end

  defp play_game(vm, score) do
    vm
    |> VM.exec()
    |> case do
      %{state: :waiting} = vm ->
        vm
        |> VM.output_to_list()
        |> do_game(score)
        |> case do
          {vm, state} -> play_game(vm, state)
          score -> score
        end

      %{state: :halted} = vm ->
        vm
        |> VM.output_to_list()
        |> elem(1)
        |> Enum.chunk_every(3)
        |> Enum.find(fn l -> hd(l) == -1 end)
        |> Enum.at(-1)
    end
  end

  defp do_game({vm, output}, state) do
    state =
      output
      |> Enum.chunk_every(3)
      |> Enum.reduce(state, fn
        [-1, 0, score], state -> %{state | score: score}
        [px, _, 3], state -> %{state | px: px}
        [bx, _, 4], state -> %{state | bx: bx}
        [x, y, 2], state -> %{state | blocks: MapSet.put(state.blocks, {x, y})}
        [x, y, 0], state -> %{state | blocks: MapSet.delete(state.blocks, {x, y})}
        _, state -> state
      end)

    # IO.inspect(state, label: :after)

    vm = VM.push_input(vm, sign(state.bx - state.px))

    if MapSet.size(state.blocks) == 0, do: state.score, else: {vm, state}
  end

  defp sign(0), do: 0
  defp sign(n) when n < 0, do: -1
  defp sign(_), do: 1
end
