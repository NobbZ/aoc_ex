defmodule Aoc.Duration do
  import Kernel, except: [to_string: 1]

  def to_string(d) when is_struct(d),
    do: d |> Timex.Duration.normalize() |> Timex.Duration.to_clock() |> to_string()

  def to_string({0, 0, 0, μs}) when μs < 1000, do: "#{μs}µs"
  def to_string({0, 0, 0, μs}), do: "#{div(μs, 1000)}ms #{rem(μs, 1000)}µs"
  def to_string({0, 0, s, μs}), do: "#{s}s #{div(μs, 1000)}ms"
  def to_string({0, m, s, _}), do: "#{m}m #{s}s"
end
