defmodule AocEx.Y2021.D1 do
  use AocEx.Boilerplate,
    file: "priv/y2021/d01.txt",
    transform: fn raw -> raw |> String.split() |> Enum.map(&String.to_integer/1) end

  def part1(input \\ processed()) do
    input
    |> Enum.chunk_every(2, 1, :discard)
    |> Enum.count(fn [a, b] -> a < b end)
  end

  def part2(input \\ processed()) do
    input
    |> Enum.chunk_every(3, 1, :discard)
    |> Enum.map(&Enum.sum/1)
    |> part1()
  end
end
