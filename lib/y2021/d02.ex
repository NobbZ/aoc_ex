defmodule AocEx.Y2021.D2 do
  use AocEx.Boilerplate,
    file: "priv/y2021/d02.txt",
    transform: fn raw ->
      raw
      |> String.split("\n")
      |> Enum.map(fn l ->
        l
        |> String.split()
        |> then(fn [dir, step] -> {String.to_atom(dir), String.to_integer(step)} end)
      end)
    end

  def part1(input \\ processed()) do
    input
    |> Enum.reduce({0, 0}, fn
      {:forward, step}, {horizonal, depth} -> {horizonal + step, depth}
      {:down, step}, {horizontal, depth} -> {horizontal, depth + step}
      {:up, step}, {horizontal, depth} -> {horizontal, depth - step}
    end)
    |> then(fn {horizontal, step} -> horizontal * step end)
  end

  def part2(input \\ processed()) do
    input
    |> Enum.reduce({0, 0, 0}, fn
      {:forward, step}, {hor, depth, aim} -> {hor + step, depth + step * aim, aim}
      {:down, step}, {hor, depth, aim} -> {hor, depth, aim + step}
      {:up, step}, {hor, depth, aim} -> {hor, depth, aim - step}
    end)
    |> then(fn {hor, depth, _} -> hor * depth end)
  end
end
